<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
hhhhh
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<?php if( get_field('price_product') ): ?>
					<h2><?php the_field('price_product'); ?></h2>
				<?php endif; ?>

				<?php if( get_field('color') ): ?>
					<h2><?php the_field('color'); ?></h2>
				<?php endif; ?>
				<div style="color: <?php the_field('color');?>">kokoko</div>

				<?php get_template_part( 'content', get_post_format() ); ?>
				<?php twentythirteen_post_nav(); ?>
				<?php comments_template(); ?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>