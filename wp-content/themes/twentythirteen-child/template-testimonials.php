<?php
/**
 * The template for displaying Category pages
 * Template Name: Testimonials
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		<?php 
 				  $args = array(
                   'post_type' => 'product',
                   'publish' => true,
                   'paged' => get_query_var('paged'),
               );
            
            query_posts($args);

          


		if ( have_posts()  ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( 'Category Archives: %s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></h1>

				<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
				<?php endif; ?>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<?php if( get_field('price_product') ): ?>
					<h2><?php the_field('price_product'); ?></h2>
				<?php endif; ?>

				<?php if( get_field('color') ): ?>
					<h2><?php the_field('color'); ?></h2>
				<?php endif; ?>
				<div style="color: <?php the_field('color');?>">kokoko</div>


				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
