<?php
add_action( 'init', 'create_posttype' );
function create_posttype() {
  register_post_type( 'product',
    array(
      'labels' => array(
        'name' => __( 'Product' ),
        'singular_name' => __( 'Product' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'product'),
    )
  );
}
?>