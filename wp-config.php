<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wphomepage');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+60wa~DEsCN[f?^C}Uic[9i:CeAaV1)e2NPEW+oFYEx<Bf^[U]!X?a_DO/wQ!T%J');
define('SECURE_AUTH_KEY',  'f>0+J>6-[-LW4h&d7_6? L%14-2$E+#=+}HE >PY]3&QG[=1D)[~Xtoa3@}c!9/}');
define('LOGGED_IN_KEY',    'Y1GY2,1wZCA3OP<-M5t.hFeu0Fw+O=|{ oW[oM{K?-!155Tz+h?2.8gg~8.1Mr>!');
define('NONCE_KEY',        'aG.J<T)Nvl=+>/c>l/:P5e,cYi!34! cUW#27wlo>aH}{A!jwHZL-F7rUsS8BDUX');
define('AUTH_SALT',        'yQ^]&t(f|mfkC==Jz@ALAc[(t+Rb##Tg*66LFHXhzqMlVaB9#Ze{vY9bn_7hPrk+');
define('SECURE_AUTH_SALT', ',S][L,)>N-NcoZ|ZpZD-@Pu}3(56GNv55.o>$Dv((>!hwn`r]p^lrh2u5n^FwfP ');
define('LOGGED_IN_SALT',   ' w0D`DNe.~L<,v~$Wpd2&>%8^#_,+i(R Aop&j=cWWLDTqr(mY=`A<{b5x57+k~j');
define('NONCE_SALT',       '+oTG=:4XX35Glh}#]|6[BhZ(f*1uz?u|HGIy7Fy|?c=~iA]+-p<JY8QS32Xz29ij');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
